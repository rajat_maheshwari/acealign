class Exception extends Error {
    statusCode;
    message;

    constructor(
        statusCode,
        message,
    ) {
        super();
        this.statusCode = statusCode;
        this.message = message;
    }
}

module.exports = Exception;
