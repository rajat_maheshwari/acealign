const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const doctorSchema = new Schema({
    username: { type: String, require: true, index: true, },
    password: { type: String, require: true, },
}, {
    versionKey: false,
    timestamps: true,
});

module.exports = mongoose.model("doctor", doctorSchema);
