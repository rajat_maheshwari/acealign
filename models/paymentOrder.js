const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const paymentOrderSchema = new Schema({
    name: { type: String, require: true, index: true, },
    mobileNo: { type: String, require: true, index: true, },
    email: { type: String, require: true, index: true, },
    amount: { type: Number, require: true, },
    currency: { type: String, require: true, enum: ["INR", "USD"], },
    email: { type: String, require: true, index: true, },
    members: {
        name1: { type: String, require: false, },
        name3: { type: String, require: false, },
        name2: { type: String, require: false, },
    },
    remainingCoverageAmount: { type: Number, required: true, },
    isPlanActive: { type: Boolean, default: false, index: true, },
    orderId: { type: String, require: true, index: true, },
    paymentId: { type: String, index: true, },
    membershipId: { type: String, unique: true, require: true, index: true, trim: true, },
    orderDate: { type: Date, require: true, index: true, },
    paymentDate: { type: Date, index: true, },
}, {
    versionKey: false,
    timestamps: true,
});

module.exports = mongoose.model("paymentOrder", paymentOrderSchema);
