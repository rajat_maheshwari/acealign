const Exception = require("../helpers/Exception");

// These represent the incoming data containers that we might need to validate
const containers = {
    query: {
        storageProperty: 'originalQuery',
        joi: {
            convert: true,
            allowUnknown: false,
            abortEarly: false
        }
    },
    // For use with body-parser
    body: {
        storageProperty: 'originalBody',
        joi: {
            convert: true,
            allowUnknown: false,
            abortEarly: false
        }
    },
    headers: {
        storageProperty: 'originalHeaders',
        joi: {
            convert: true,
            allowUnknown: true,
            stripUnknown: false,
            abortEarly: false
        }
    },
    // URL params e.g "/users/:userId"
    params: {
        storageProperty: 'originalParams',
        joi: {
            convert: true,
            allowUnknown: false,
            abortEarly: false
        }
    },
    // For use with express-formidable or similar POST body parser for forms
    fields: {
        storageProperty: 'originalFields',
        joi: {
            convert: true,
            allowUnknown: false,
            abortEarly: false
        }
    }
};

const schemaValidator = (schema) => {
    return (req, res, next) => {
        try {
            ["headers", "params", "query", "body", "fields"]
                .forEach((key) => {
                    const joiSchema = schema[key];
                    if (joiSchema) {
                        const { error, value } = joiSchema.validate(req[key], containers[key].joi);
                        if (error) {
                            const message = error.details[0].message.replace(/"/g, "");
                            throw new Exception(400, message);
                        } else {
                            req[key] = value;
                        }
                    }
                });
                next();
        } catch (error) {
            res
                .status(error.statusCode || 500)
                .send({
                    statusCode: error.statusCode || 500,
                    message: error.message,
                });
        }
    }
};

module.exports = schemaValidator;
