const express = require("express");
const Joi = require("joi");
const cors = require("cors");
const mongoose = require("mongoose");
const Razorpay = require("razorpay");

const schemaValidator = require("./middlewares/schemaValidator");
const PaymentOrder = require("./models/paymentOrder");
const Doctor = require("./models/doctor");
const generateString = require("./helpers/generateString");
const Exception = require("./helpers/Exception");

mongoose
    .connect("mongodb+srv://thelittledentistcare:lc5vI4a7cAHIHGX1@dentlite.8wzply9.mongodb.net/acealign?retryWrites=true&w=majority")
    .then(() => console.log("Successfully connected to Mongodb Atlas DB."))
    .catch(() => console.log("Failed to connect to Mongodb Atlas DB."));
mongoose.set("debug", true);

const app = express();
app.use(express.json());
app.use(cors());

const createOrderSchema = {
    body: Joi.object({
        name: Joi.string().trim().required(),
        mobileNo: Joi.string()
            .trim()
            .required()
            .messages({
                "any.required": "mobile number is required",
                "string.empty": "mobile number is required",
            }),
        email: Joi.string()
            .trim()
            .lowercase()
            .email({ minDomainSegments: 2, })
            .regex(/[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,63}$/)
            .required()
            .messages({
                "string.pattern.base": "please enter a valid email",
            }),
        amount: Joi.number().required(),
        currency: Joi.string().trim().valid("INR", "USD").required(),
        remainingCoverageAmount: Joi.number().required(),
        members: Joi.object({
            name1: Joi.string().trim().optional().allow(""),
            name2: Joi.string().trim().optional().allow(""),
            name3: Joi.string().trim().optional().allow(""),
        }).optional(),
    }),
};

const capturePaymentSchema = {
    params: Joi.object({
        orderId: Joi.string().trim().required(),
    }),
    body: Joi.object({
        paymentId: Joi.string().trim().required(),
    }),
};

const doctorLoginSchema = {
    body: Joi.object({
        username: Joi.string().trim().required(),
        password: Joi.string().trim().required(),
    }),
};

const paymentOrderSearchSchema = {
    query: Joi.object({
        mobileNo: Joi.string().trim().optional(),
        membershipId: Joi.string().trim().optional(),
    }).or("mobileNo", "membershipId"),
};

app.post("/api/createorder", schemaValidator(createOrderSchema), async function (req, res) {
    try {
        const instance = new Razorpay({ key_id: "rzp_live_KG6zxKU8CINWyw", key_secret: "8g0QONLWXsNQupzsAcOHDPiJ", });
        const orderData = await instance.orders.create({
            amount: req.body.amount,
            currency: req.body.currency,
            notes: {
                "name": req.body.name,
                "mobile": req.body.mobileNo,
            },
        });
        const paymentOrder = new PaymentOrder({
            ...req.body,
            orderId: orderData.id,
            orderDate: new Date(),
            membershipId: generateString(6),
        });
        const paymentOrderData = await paymentOrder.save(paymentOrder);
        console.log("orderData===============>", orderData);
        res.status(200).send({ statusCode: 200, data: paymentOrderData, });
    } catch (error) {
        console.log("/api/createorder error", error);
        res
            .status(error.statusCode || 500)
            .send({
                statusCode: error.statusCode || 500,
                message: error.message || error.error.description,
            });
    }
});

app.post("/api/capturepayment/:orderId", schemaValidator(capturePaymentSchema), async function (req, res) {
    try {
        const isExists = await PaymentOrder.findOne({ orderId: req.params.orderId, });
        if (!isExists) {
            throw new Exception(400, "Order id does not exists");
        } else if (isExists.paymentId) {
            throw new Exception(400, "Payment already done for this order");
        }
        const paymentOrderData = await PaymentOrder.findOneAndUpdate(
            { orderId: req.params.orderId, },
            { "$set": { isPlanActive: true, paymentId: req.body.paymentId, paymentDate: new Date(), } },
            { new: true },
        );
        res.status(200).send({ statusCode: 200, data: paymentOrderData, });
    } catch (error) {
        console.log("/api/capturepayment/:orderId error", error);
        res
            .status(error.statusCode || 500)
            .send({
                statusCode: error.statusCode || 500,
                message: error.message,
            });
    }
});

app.post("/api/doctor/login", schemaValidator(doctorLoginSchema), async function (req, res) {
    try {
        const isExists = await Doctor.findOne({ ...req.body, });
        if (!isExists) {
            throw new Exception(400, "Please enter valud Username or password");
        }
        res.status(200).send({ statusCode: 200, data: {}, });
    } catch (error) {
        console.log("/api/doctor/login error", error);
        res
            .status(error.statusCode || 500)
            .send({
                statusCode: error.statusCode || 500,
                message: error.message,
            });
    }
});

app.get("/api/paymentorder/search", schemaValidator(paymentOrderSearchSchema), async function (req, res) {
    try {
        const paymentOrderData = await PaymentOrder.find({ ...req.query, }, { createdAt: 0, updatedAt: 0, });
        res.status(200).send({ statusCode: 200, data: paymentOrderData, });
    } catch (error) {
        console.log("/api/paymentorder/search error", error);
        res
            .status(error.statusCode || 500)
            .send({
                statusCode: error.statusCode || 500,
                message: error.message,
            });
    }
});

app.get("/health", (req, res) => res.sendStatus(200));

app.listen(8080, function () {
    console.log("Application running on port 8080");
});


// mongodb + srv://thelittledentistcare:thelittledentistcare@dentlite.8wzply9.mongodb.net/